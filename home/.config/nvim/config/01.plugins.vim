" Load the plugins
call plug#begin()

" Molokai color theme
Plug 'tomasr/molokai'

" Autocompletion
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

" Language client for RLS
Plug 'autozimu/LanguageClient-neovim', { 'branch': 'next', 'do': 'make release', }

" Show and remove extra whitespace
Plug 'ntpeters/vim-better-whitespace'

" Fuzzy find
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Automatically use correct tabs/spaces when defined
Plug 'editorconfig/editorconfig-vim'

" Rust formatting
Plug 'rust-lang/rust.vim'
" Rust code completion
Plug 'racer-rust/vim-racer'

" C++ highlighting
Plug 'octol/vim-cpp-enhanced-highlight'

" Lua indentation
Plug 'raymond-w-ko/vim-lua-indent'

" Git
Plug 'tpope/vim-fugitive'
" Git sidebar
Plug 'airblade/vim-gitgutter'

" JSON
Plug 'elzr/vim-json'

" Markdown
Plug 'plasticboy/vim-markdown'

" TOML
Plug 'cespare/vim-toml'

" Powershell
Plug 'PProvost/vim-ps1'

" Cap'n Proto
Plug 'peter-edge/vim-capnp'

" Perl 5 & 6
Plug 'vim-perl/vim-perl'

" Status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Fancy icons
Plug 'ryanoasis/vim-devicons'

" Tables
Plug 'dhruvasagar/vim-table-mode'

call plug#end()
