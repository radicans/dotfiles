" Use the molokai plugin colorscheme
colorscheme molokai

" Map fuzzy finding to Ctrl-P in all modes
map <C-p> :GFiles<CR>
map! <C-p> :GFiles<CR>
map <C-.> :FZF<CR>
map! <C-.> :FZF<CR>

set hidden

" Language server commands
let g:LanguageClient_serverCommands = {
            \ 'rust': ['/home/thomas/.cargo/bin/rls'],
\ }
let g:LanguageClient_autoStart=1

nnoremap <F5> :call LanguageClient_contextMenu()<CR>
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
" Show type info under cursor
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
" Format code in normal mode
nnoremap <silent> gf :call LanguageClient_textDocument_formatting()<cr>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

" Use deoplete
let g:deoplete#enable_at_startup=1
let g:deoplete#auto_complete_delay=0
let g:deoplete#auto_complete_start_length=1
" Use tab to forward cycle through the autocompletion menu
inoremap <silent><expr><C-j> pumvisible() ? "\<c-n>" : "\<tab>"
inoremap <silent><expr><C-k> pumvisible() ? "\<c-p>" : "\<s-tab>"

" Don't fold the top header
let g:vim_markdown_folding_level=2

" Set the airline theme
let g:airline_theme='tomorrow'
let g:airline_powerline_fonts=1

" Automatically remove extra whitespace
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1

" Show the complete function definition
let g:racer_experimental_completer=1
